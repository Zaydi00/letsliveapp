﻿$(document).ready(function () {
    ClearInputs();
    DropdownFiller();
})

//Button Click
$('#btnSignup').click(function (e) {
    e.preventDefault();

    //Inputs Mapper Variables
    var _name = $('#txtname').val();
    var _email = $('#txtemail').val();
    var _phone = $('#txtphone').val();
    var _password = $('#txtpass').val();
    var _conpassword = $('#txtconpass').val();
    var _gender = $('#txtgender').val();
    var _city = $('#txtcity').val();

    //Input Validations
    if (_name == "") {
        swal({
            title: "Error",
            text: "Please enter your Full Name",
            icon: "warning",
            timer: 1000,
            showConfirmButton: false
        })
    }
    else if (_email == "") {
        swal({
            title: "Error",
            text: "Please enter your email address",
            icon: "warning",
            timer: 1000,
            showConfirmButton: false
        })
    }
    else if (_phone == "") {
        swal({
            title: "Error",
            text: "Please enter your Phone Number",
            icon: "warning",
            timer: 1000,
            showConfirmButton: false
        })
    }
    else if (_password == "") {
        swal({
            title: "Error",
            text: "Please enter your password",
            icon: "warning",
            timer: 1000,
            showConfirmButton: false
        })
    }
    else if (_conpassword != _password) {
        swal({
            title: "Error",
            text: "Password does not match",
            icon: "warning",
            timer: 1000,
            showConfirmButton: false
        })
    }
    else if (_gender == "") {
        swal({
            title: "Error",
            text: "Please select your gender",
            icon: "warning",
            timer: 1000,
            showConfirmButton: false
        })
    }
    else if (_city == "") {
        swal({
            title: "Error",
            text: "Please select your city",
            icon: "warning",
            timer: 1000,
            showConfirmButton: false
        })
    }
    else {
        //Ajax Call for Signup Users
        $.ajax({
            type: "POST",
            url: '/UserAccounts/Signup/',
            contentType: "application/json",
            traditional: true,
            data: JSON.stringify({ name: _name, email: _email, phone: _phone, password: _conpassword, gender: _gender, city: _city }),
            dataType: "json",
            success: function (recData) {
                if (recData.status == "success") {

                    //Message Success
                    swal({
                        title: "Done!",
                        text: recData.data,
                        icon: "success",
                        timer: 1000,
                        showConfirmButton: false
                    });
                    window.location.href = "/Home/";
                    ClearInputs();
                }
                else {
                    swal({
                        title: "Error!",
                        text: recData.data,
                        icon: "error",
                        timer: 1000,
                        showConfirmButton: false
                    });
                }
            },
            error: function () {
                swal({
                    title: "Error!",
                    text: recData.data,
                    icon: "error",
                    timer: 1000,
                    showConfirmButton: false
                });
            }
        })
    }
});
//City DropDown
function DropdownFiller() {
    $.ajax({
        type: "POST",
        url: "/UserAccounts/CityDropdown",
        contentType: "application/json",
        traditional: true,
        dataType: "json",
        success: function (recData) {
            if (recData.status == "success") {
                //toastr["success"]("Done!", "Departments loaded...")
                $.each(recData.data, function (key, value) {
                    $('#txtcity').append('<option value=' + value.ID + '>' + value.Name + '</option>');
                });
            }
            else {
                toastr["error"]("Error!", "Unable to load " + name + ".");
            }

        },
        error: function () {
            toastr["error"]("Error!", "Server error on loading " + name + ".")
        }
    });
}
//Clearing Inputs
function ClearInputs() {
    $('#txtname').val('');
    $('#txtemail').val('');
    $('#txtphone').val('');
    $('#txtpass').val('');
    $('#txtconpass').val('');
    $('#txtgender').val(0);
    $('#txtcity').val(0);
}