﻿using LetsLive.Controllers.Settings;
using LetsLive.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LetsLive.Controllers.WebsiteUsers
{
    public class UserAccountsController : Controller
    {
        [HttpPost]
        public ActionResult Signup(int id, string name, string email, string phone, string password, string gender, string city)
        {
            try
            {
                using (ConnectionStr db = new ConnectionStr())
                {
                    WebsiteUser u = new WebsiteUser();
                    u.ID = ProgramSettings.assignId("WebsiteUsers", "ID", "");
                    u.FullName = name;
                    u.EmailAddress = email;
                    u.PhoneNumber = phone;
                    u.Password = password;
                    u.Gender = gender;
                    u.City = city;
                    u.AvailabilityStatus = true;
                    u.CreationDate = DateTime.Now.ToShortDateString();
                    db.WebsiteUsers.Add(u);
                    db.SaveChanges();
                    return Json(new { status = "success", data = "Successfully Signed Up" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception x)
            {
                return Json(new { status = "error", data = x.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult CityDropdown()
        {
            try
            {
                using (ConnectionStr db = new ConnectionStr())
                {
                    var q = (from us in db.Cities
                             select us).ToList();
                    if (q !=null)
                    {
                        return Json(new { status = "success", data = q }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { status = "error", data = "No Cities Found" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception x)
            {
                return Json(new { status = "error", data = x.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}